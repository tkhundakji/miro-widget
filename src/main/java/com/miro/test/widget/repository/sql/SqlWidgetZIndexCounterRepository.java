package com.miro.test.widget.repository.sql;

import com.miro.test.widget.repository.sql.entity.ZIndexCounterEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;

@Profile("sql")
public interface SqlWidgetZIndexCounterRepository extends CrudRepository<ZIndexCounterEntity, Integer> {
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query(value = "SELECT R FROM ZIndexCounterEntity R WHERE id = 1")
    ZIndexCounterEntity lockCounter();

    @Transactional
    @Modifying
    @Query(value = "UPDATE ZIndexCounterEntity R SET R.counter = R.counter + :increment")
    void incrementCounter(@Param("increment") int newCounter);

    @Transactional
    @Modifying
    @Query(value = "UPDATE ZIndexCounterEntity R SET R.counter = :new_counter")
    void updateCounterValue(@Param("new_counter") int newCounter);
}

package com.miro.test.widget.repository.memory;

import com.miro.test.widget.domain.exception.ZIndexOutOfBoundException;
import com.miro.test.widget.domain.internal.CoordinateModel;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import com.miro.test.widget.repository.WidgetRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
@Profile("memory")
public class MemoryWidgetRepositoryImpl implements WidgetRepositoryInterface {

    private final SortedMap<Integer, WidgetModel> sortedWidgets;
    private final Map<UUID, WidgetModel> idToWidgetMap;
    private final ReadWriteLock readWriteLock;
    //The reason why the increment is not set to 1, is to increase the like hood of not needing to shift.
    private final Integer zValueIncrement = 100;
    private Integer currentZValue = 0;

    @Autowired
    public MemoryWidgetRepositoryImpl() {
        sortedWidgets = new TreeMap<>();
        idToWidgetMap = new HashMap<>();
        readWriteLock = new ReentrantReadWriteLock();
    }

    //We store the widget models in a tree map
    //Adding an element that does not exist is done in o(log n) where n is the size of the map
    //Adding an element that does exist is done in o(m * log n) where m is the number of shifts needed and n is the size of the map.
    //Adding an element to the end is done in o(log n) where n is the size of the map
    @Override
    public WidgetModel store(WidgetModel widgetModel) {
        try {
            readWriteLock.writeLock().lock();
            widgetModel.setLastModified(LocalDate.ofInstant(Instant.now(), ZoneId.of("UTC")));

            // In case the key does not exists, calculate a new index and add the value
            if (widgetModel.getZIndex() == null) {
                //in case we ran out of z index space, we need to stop the process
                if (currentZValue >= Integer.MAX_VALUE - zValueIncrement) {
                    throw new ZIndexOutOfBoundException();
                }
                currentZValue = currentZValue + zValueIncrement;
                widgetModel.setZIndex(currentZValue);
            } else {
                currentZValue = Math.max(currentZValue, widgetModel.getZIndex());
                // In case the key exists shift the values
                if (sortedWidgets.containsKey(widgetModel.getZIndex())) {
                    Integer currentIndex = widgetModel.getZIndex();

                    shiftWidgets(currentIndex);
                }
            }

            sortedWidgets.put(widgetModel.getZIndex(), widgetModel);
            idToWidgetMap.put(widgetModel.getId(), widgetModel);
            return widgetModel;
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    private void shiftWidgets(Integer fromIndex) {
        // to make sure we have enough space to shift
        if (currentZValue >= Integer.MAX_VALUE - 1) {
            throw new ZIndexOutOfBoundException();
        }

        WidgetModel nextValue = null;
        WidgetModel currentValue = sortedWidgets.get(fromIndex);

        while (sortedWidgets.containsKey(fromIndex + 1)) {
            nextValue = sortedWidgets.get(fromIndex + 1);
            sortedWidgets.put(fromIndex + 1, currentValue);
            currentValue.setZIndex(currentValue.getZIndex() + 1);
            currentValue = nextValue;
            fromIndex++;
        }
        currentValue.setZIndex(currentValue.getZIndex() + 1);
        currentZValue = Math.max(fromIndex + 1, currentZValue);
        sortedWidgets.put(fromIndex + 1, currentValue);
    }

    // Updating a widget without changing the z index is done in o(1)
    // updating a widget with changing the z index is done in o(m * log n ) where m is the number of shifts needed and n is the size of the map
    @Override
    public Optional<WidgetModel> update(UUID id, UpdateWidgetRequest updateWidgetRequest) {
        WidgetModel newWidgetModel = null;
        try {
            readWriteLock.writeLock().lock();

            if (!idToWidgetMap.containsKey(id)) {
                return Optional.empty();
            }

            WidgetModel widgetModel = idToWidgetMap.get(id);

            newWidgetModel = calculatedUpdatedWidgetModel(updateWidgetRequest, widgetModel.clone());

            if (!newWidgetModel.getZIndex().equals(widgetModel.getZIndex())) {
                this.store(newWidgetModel);

                // if the node to be updated is the last node then we can reduce the z index
                if (sortedWidgets.tailMap(widgetModel.getZIndex() + 1).size() == 0
                        && newWidgetModel.getZIndex() < widgetModel.getZIndex()) {

                    sortedWidgets.remove(widgetModel.getZIndex());
                    currentZValue = sortedWidgets.lastKey() + 1;
                } else {
                    sortedWidgets.remove(widgetModel.getZIndex());
                }
            } else {
                sortedWidgets.put(newWidgetModel.getZIndex(), newWidgetModel);
                idToWidgetMap.put(newWidgetModel.getId(), newWidgetModel);
            }

        } finally {
            readWriteLock.writeLock().unlock();
        }
        return Optional.of(newWidgetModel);
    }

    // deleting a widget by id is done in o (log n) where n is the size of the map
    @Override
    public boolean deleteWidgetById(UUID id) {
        try {
            readWriteLock.writeLock().lock();

            if (!idToWidgetMap.containsKey(id)) {
                return false;
            }

            WidgetModel widgetModel = idToWidgetMap.get(id);

            sortedWidgets.remove(widgetModel.getZIndex());
            idToWidgetMap.remove(widgetModel.getId());

            // if the current node is the last node
            if (sortedWidgets.size() > 0 && sortedWidgets.tailMap(widgetModel.getZIndex() + 1).size() == 0) {
                currentZValue = sortedWidgets.lastKey() + 1;
            }
            if (sortedWidgets.size() == 0) {
                currentZValue = 0;
            }
            return true;
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    // fetching a widget by id is done in o(1)
    @Override
    public Optional<WidgetModel> getWidgetById(UUID id) {
        WidgetModel widgetModel = null;
        try {
            readWriteLock.readLock().lock();
            if (!idToWidgetMap.containsKey(id)) {
                return Optional.empty();
            }
            widgetModel = idToWidgetMap.get(id);
        } finally {
            readWriteLock.readLock().unlock();
        }
        return Optional.of(widgetModel);
    }

    // fetching a widget by z index is done in o(n) where n is the number of widgets requested + log m where m is the widget map
    @Override
    public List<WidgetModel> getWidgetsByZIndex(@Nullable Integer lastZIndex, Integer limit) {
        List<WidgetModel> widgets = new ArrayList<>();
        try {
            readWriteLock.readLock().lock();
            try {
                Iterator<WidgetModel> mapItr;
                if (lastZIndex == null) {
                    mapItr = sortedWidgets.values().iterator();
                } else {
                    mapItr = sortedWidgets.tailMap(lastZIndex + 1).values().iterator();
                }
                for (int i = limit; i > 0 && mapItr.hasNext(); i--) {
                    widgets.add(mapItr.next());
                }
            } catch (NoSuchElementException e) {
                return widgets;
            }
        } finally {
            readWriteLock.readLock().unlock();
        }
        return widgets;
    }

    @Override
    public void deleteAll() {
        try {
            readWriteLock.readLock().lock();
            idToWidgetMap.clear();
            sortedWidgets.clear();
            currentZValue = 0;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    private WidgetModel calculatedUpdatedWidgetModel(UpdateWidgetRequest updateWidgetRequest, WidgetModel widgetModel) {
        WidgetModel newWidgetModel;
        newWidgetModel = widgetModel;

        if (updateWidgetRequest.getWidth() != null) {
            newWidgetModel.setWidth(updateWidgetRequest.getWidth());
        }

        if (updateWidgetRequest.getHeight() != null) {
            newWidgetModel.setHeight(updateWidgetRequest.getHeight());
        }

        if (updateWidgetRequest.getZIndex() != null) {
            newWidgetModel.setZIndex(updateWidgetRequest.getZIndex());
        }

        if (updateWidgetRequest.getCoordinates() != null) {
            CoordinateModel coordinates = newWidgetModel.getCoordinates();
            if (updateWidgetRequest.getCoordinates().getX() != null) {
                coordinates.setX(updateWidgetRequest.getCoordinates().getX());
            }

            if (updateWidgetRequest.getCoordinates().getY() != null) {
                coordinates.setY(updateWidgetRequest.getCoordinates().getY());
            }
        }
        return newWidgetModel;
    }
}

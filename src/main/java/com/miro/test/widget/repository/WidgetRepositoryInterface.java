package com.miro.test.widget.repository;

import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public interface WidgetRepositoryInterface {
    WidgetModel store(WidgetModel widgetModel);

    Optional<WidgetModel> update(UUID id, UpdateWidgetRequest widgetModel);

    boolean deleteWidgetById(UUID id);

    Optional<WidgetModel> getWidgetById(UUID id);

    List<WidgetModel> getWidgetsByZIndex(Integer lastZIndex, Integer limit);

    void deleteAll();
}

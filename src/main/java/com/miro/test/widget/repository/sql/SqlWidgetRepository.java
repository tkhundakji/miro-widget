package com.miro.test.widget.repository.sql;

import com.miro.test.widget.repository.sql.entity.WidgetEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Profile("sql")

public interface SqlWidgetRepository extends CrudRepository<WidgetEntity, String> {
    @Query(nativeQuery = true, value = "SELECT * FROM widgets WHERE z_index > :last_index ORDER BY z_index asc limit :limit")
    List<WidgetEntity> getWidgetsByZIndex(@Param("last_index") Integer lastZIndex, @Param("limit") Integer limit);

    @Query(nativeQuery = true, value = "SELECT * FROM widgets ORDER BY z_index asc limit :limit")
    List<WidgetEntity> getWidgets(@Param("limit") Integer limit);

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<WidgetEntity> findById(String id);

    @Query("SELECT Q FROM WidgetEntity Q WHERE Q.z_index = :id")
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<WidgetEntity> findByZIndexAndLock(@Param("id") Integer zIndex);

    @Modifying
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteById(String id);

    @Modifying
    @Transactional
    @Query("DELETE FROM WidgetEntity Q WHERE Q.z_index = :zIndex")
    void deleteByZIndex(@Param("zIndex") Integer zIndex);

    @Transactional
    @Query(nativeQuery = true, value = "SELECT curr_widget.* FROM widgets curr_widget INNER JOIN (SELECT z_index - lag(z_index) OVER (ORDER BY z_index) as prev_z_index, z_index FROM widgets where z_index >= :zIndex order by prev_z_index ) prev_z_index_table ON curr_widget.z_index = prev_z_index_table.z_index where prev_z_index_table.prev_z_index > 1 for update")    Optional<WidgetEntity> findIdToShiftBefore(@Param("zIndex") Integer zIndex);

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT Q FROM WidgetEntity Q WHERE Q.z_index >= :startZIndex and Q.z_index < :endZIndex")
    List<WidgetEntity> selectIdsToShiftRange(@Param("startZIndex") Integer startZIndex, @Param("endZIndex") Integer endZIndex);

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT Q FROM WidgetEntity Q WHERE Q.z_index >= :startZIndex")
    List<WidgetEntity> selectIdToShift(@Param("startZIndex") Integer startZIndex);

    @Transactional
    @Modifying
    @Query("UPDATE WidgetEntity Q SET Q.z_index = Q.z_index + 1 WHERE Q.id in :ids")
    Integer shiftIds(@Param("ids") List<String> ids);
}

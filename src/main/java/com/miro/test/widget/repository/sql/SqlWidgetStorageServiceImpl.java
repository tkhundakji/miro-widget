package com.miro.test.widget.repository.sql;

import com.miro.test.widget.domain.exception.ZIndexOutOfBoundException;
import com.miro.test.widget.domain.internal.CoordinateModel;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import com.miro.test.widget.repository.WidgetRepositoryInterface;
import com.miro.test.widget.repository.sql.SqlWidgetRepository;
import com.miro.test.widget.repository.sql.SqlWidgetZIndexCounterRepository;
import com.miro.test.widget.repository.sql.entity.WidgetEntity;
import com.miro.test.widget.repository.sql.entity.ZIndexCounterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Profile("sql")
@Service
public class SqlWidgetStorageServiceImpl implements WidgetRepositoryInterface {

    private static final int increment = 100;

    private final SqlWidgetRepository sqlWidgetRepository;
    private final SqlWidgetZIndexCounterRepository sqlWidgetZIndexCounterRepository;

    @Autowired
    public SqlWidgetStorageServiceImpl(SqlWidgetRepository sqlWidgetRepository, SqlWidgetZIndexCounterRepository sqlWidgetZIndexCounterRepository) {
        this.sqlWidgetRepository = sqlWidgetRepository;
        this.sqlWidgetZIndexCounterRepository = sqlWidgetZIndexCounterRepository;
    }

    @Override
    @Transactional
    public WidgetModel store(WidgetModel widgetModel) {
        ZIndexCounterEntity currentCounter = sqlWidgetZIndexCounterRepository.lockCounter();
        if (currentCounter == null) {
            createCounter();
        }
        currentCounter = sqlWidgetZIndexCounterRepository.lockCounter();

        if (widgetModel.getZIndex() == null) {
            if (currentCounter.getCounter() >= Integer.MAX_VALUE - increment) {
                throw new ZIndexOutOfBoundException();
            }
            widgetModel.setZIndex(currentCounter.getCounter() + increment);
            sqlWidgetZIndexCounterRepository.incrementCounter(increment);
        } else if (currentCounter.getCounter() < widgetModel.getZIndex()) {
            sqlWidgetZIndexCounterRepository.updateCounterValue(widgetModel.getZIndex() + 1);
        }

        Optional<WidgetEntity> widgetEntity = sqlWidgetRepository.findByZIndexAndLock(widgetModel.getZIndex());

        if (widgetEntity.isPresent()) {
            Integer widgetZIndex = widgetModel.getZIndex();
            shiftValuesAndUpdateCounter(currentCounter, widgetZIndex);
        }

        WidgetEntity newWidgetEntity = mapWidgetModelToEntity(widgetModel);
        sqlWidgetRepository.save(newWidgetEntity);

        newWidgetEntity = sqlWidgetRepository.findByZIndexAndLock(widgetModel.getZIndex()).get();

        return mapWidgetEntityToModel(newWidgetEntity);
    }

    private void shiftValuesAndUpdateCounter(ZIndexCounterEntity currentCounter, Integer widgetZIndex) {

        Optional<WidgetEntity> widgetEntity = sqlWidgetRepository.findIdToShiftBefore(widgetZIndex);

        List<WidgetEntity> widgetEntities;

        if (widgetEntity.isPresent()) {
            widgetEntities = sqlWidgetRepository.selectIdsToShiftRange(widgetZIndex, widgetEntity.get().getZ_index());
        } else {
            widgetEntities = sqlWidgetRepository.selectIdToShift(widgetZIndex);
        }

        if (widgetEntities.size() > 0) {
            Integer lastZIndex = widgetEntities.get(widgetEntities.size() - 1).getZ_index();

            if (lastZIndex >= Integer.MAX_VALUE - 1) {
                throw new ZIndexOutOfBoundException();
            }

            List<String> ids = widgetEntities.stream().map(WidgetEntity::getId).collect(Collectors.toList());
            sqlWidgetRepository.shiftIds(ids);
            sqlWidgetRepository.deleteByZIndex(widgetZIndex);

            if (currentCounter.getCounter() < lastZIndex + 1) {
                sqlWidgetZIndexCounterRepository.updateCounterValue(lastZIndex + 1);
            }
        }
    }

    @Override
    @Transactional
    public Optional<WidgetModel> update(UUID id, UpdateWidgetRequest widgetModel) {
        Optional<WidgetEntity> entity = sqlWidgetRepository.findById(id.toString());
        if (entity.isEmpty()) {
            return Optional.empty();
        }

        WidgetEntity newWidgetEntity = calculatedUpdatedWidgetModel(widgetModel, entity.get());

        newWidgetEntity.setLastModifiedAt(Date.valueOf(LocalDate.ofInstant(Instant.now(), ZoneId.of("UTC"))));

        if (widgetModel.getZIndex() != null) {
            sqlWidgetRepository.delete(entity.get());
            this.store(mapWidgetEntityToModel(newWidgetEntity));
        } else {
            sqlWidgetRepository.save(newWidgetEntity);
        }

        entity = sqlWidgetRepository.findById(id.toString());

        return entity.map(this::mapWidgetEntityToModel);
    }

    @Override
    public boolean deleteWidgetById(UUID id) {

        Optional<WidgetEntity> entity = sqlWidgetRepository.findById(id.toString());
        if (entity.isEmpty()) {
            return false;
        }
        sqlWidgetRepository.delete(entity.get());
        return true;
    }

    @Override
    public Optional<WidgetModel> getWidgetById(UUID id) {

        Optional<WidgetEntity> widgetEntityOptional = sqlWidgetRepository.findById(id.toString());

        return widgetEntityOptional.map(this::mapWidgetEntityToModel);
    }

    @Override
    public List<WidgetModel> getWidgetsByZIndex(Integer lastZIndex, Integer limit) {

        List<WidgetEntity> widgetEntities;

        if (lastZIndex != null) {
            widgetEntities = sqlWidgetRepository.getWidgetsByZIndex(lastZIndex, limit);
        } else {
            widgetEntities = sqlWidgetRepository.getWidgets(limit);

        }

        List<WidgetModel> widgetModels = new ArrayList<>();

        for (WidgetEntity widgetEntity : widgetEntities) {
            widgetModels.add(mapWidgetEntityToModel(widgetEntity));
        }

        return widgetModels;
    }

    @Override
    public void deleteAll() {
        sqlWidgetRepository.deleteAll();
        sqlWidgetZIndexCounterRepository.updateCounterValue(0);
    }

    private WidgetModel mapWidgetEntityToModel(WidgetEntity widgetEntity) {

        return WidgetModel.WidgetModelBuilder.aWidgetModel()
                .withWidth(widgetEntity.getWidth())
                .withHeight(widgetEntity.getHeight())
                .withId(UUID.fromString(widgetEntity.getId()))
                .withZIndex(widgetEntity.getZ_index())
                .withLastModified(widgetEntity.getLastModifiedAt() != null ? widgetEntity.getLastModifiedAt().toLocalDate() : null)
                .withCoordinates(CoordinateModel.CoordinateModelBuilder.aCoordinateModel()
                        .withX(widgetEntity.getX())
                        .withY(widgetEntity.getY())
                        .build())
                .build();
    }


    private WidgetEntity mapWidgetModelToEntity(WidgetModel widgetModel) {
        WidgetEntity newWidgetEntity = new WidgetEntity();

        newWidgetEntity.setWidth(widgetModel.getWidth());
        newWidgetEntity.setHeight(widgetModel.getHeight());
        newWidgetEntity.setX(widgetModel.getCoordinates().getX());
        newWidgetEntity.setY(widgetModel.getCoordinates().getY());
        newWidgetEntity.setZ_index(widgetModel.getZIndex());
        newWidgetEntity.setId(widgetModel.getId().toString());
        newWidgetEntity.setLastModifiedAt(Date.valueOf(LocalDate.ofInstant(Instant.now(), ZoneId.of("UTC"))));

        return newWidgetEntity;
    }

    private WidgetEntity calculatedUpdatedWidgetModel(UpdateWidgetRequest updateWidgetRequest, WidgetEntity newWidgetModel) {
        if (updateWidgetRequest.getWidth() != null) {
            newWidgetModel.setWidth(updateWidgetRequest.getWidth());
        }

        if (updateWidgetRequest.getHeight() != null) {
            newWidgetModel.setHeight(updateWidgetRequest.getHeight());
        }

        if (updateWidgetRequest.getZIndex() != null) {
            newWidgetModel.setZ_index(updateWidgetRequest.getZIndex());
        }

        if (updateWidgetRequest.getCoordinates() != null) {
            if (updateWidgetRequest.getCoordinates().getX() != null) {
                newWidgetModel.setX(updateWidgetRequest.getCoordinates().getX());
            }

            if (updateWidgetRequest.getCoordinates().getY() != null) {
                newWidgetModel.setY(updateWidgetRequest.getCoordinates().getY());
            }
        }
        return newWidgetModel;
    }

    private void createCounter() {
        ZIndexCounterEntity zIndexCounterEntity = new ZIndexCounterEntity(1, 0);
        sqlWidgetZIndexCounterRepository.save(zIndexCounterEntity);
    }
}

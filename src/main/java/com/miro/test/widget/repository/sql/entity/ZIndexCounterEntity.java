package com.miro.test.widget.repository.sql.entity;

import org.springframework.context.annotation.Profile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "z_index_counter")
@Profile("sql")
public class ZIndexCounterEntity {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "counter")
    private Integer counter;

    public ZIndexCounterEntity() {
    }

    public ZIndexCounterEntity(Integer id, Integer counter) {
        this.id = id;
        this.counter = counter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }
}

package com.miro.test.widget.domain.request;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class CoordinateRequest implements Serializable {

    @NotNull(message = "X-Coordinate must be present")
    private Integer x;

    @NotNull(message = "Y-Coordinate must be present")
    private Integer y;

    public CoordinateRequest() {
    }

    public CoordinateRequest(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinateRequest that = (CoordinateRequest) o;
        return x.equals(that.x) && y.equals(that.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}

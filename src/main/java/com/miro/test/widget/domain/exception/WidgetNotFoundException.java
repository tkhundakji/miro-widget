package com.miro.test.widget.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class WidgetNotFoundException extends RuntimeException {
    public WidgetNotFoundException(UUID id) {
        super(String.format("Widget with Id %s was not found", id.toString()));
    }
}

package com.miro.test.widget.domain.internal;

import java.io.Serializable;
import java.util.Objects;

public class CoordinateModel implements Cloneable, Serializable {

    private Integer x;
    private Integer y;

    public CoordinateModel(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public CoordinateModel() {
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public CoordinateModel clone() {
        try {
            return (CoordinateModel) super.clone();
        } catch (CloneNotSupportedException e) {
            return new CoordinateModel(this.getX(), this.getY());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinateModel that = (CoordinateModel) o;
        return Objects.equals(x, that.x) && Objects.equals(y, that.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public static final class CoordinateModelBuilder {
        private Integer x;
        private Integer y;

        private CoordinateModelBuilder() {
        }

        public static CoordinateModelBuilder aCoordinateModel() {
            return new CoordinateModelBuilder();
        }

        public CoordinateModelBuilder withX(Integer x) {
            this.x = x;
            return this;
        }

        public CoordinateModelBuilder withY(Integer y) {
            this.y = y;
            return this;
        }

        public CoordinateModel build() {
            return new CoordinateModel(x, y);
        }
    }
}

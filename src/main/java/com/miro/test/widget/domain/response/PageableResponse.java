package com.miro.test.widget.domain.response;

import java.util.List;

public class PageableResponse<T> {
    private List<T> data;
    private Integer lastValueId;
    private Integer limit;

    public PageableResponse(List<T> data, Integer lastValueId, Integer limit) {
        this.data = data;
        this.lastValueId = lastValueId;
        this.limit = limit;
    }

    public PageableResponse() {
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Integer getLastValueId() {
        return lastValueId;
    }

    public void setLastValueId(Integer lastValueId) {
        this.lastValueId = lastValueId;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }


    public static final class PageableResponseBuilder<T> {
        private List<T> data;
        private Integer lastValueId;
        private Integer limit;

        private PageableResponseBuilder() {
        }

        public static PageableResponseBuilder aPageableResponse() {
            return new PageableResponseBuilder();
        }

        public PageableResponseBuilder withData(List<T> data) {
            this.data = data;
            return this;
        }

        public PageableResponseBuilder withLastValueId(Integer lastValueId) {
            this.lastValueId = lastValueId;
            return this;
        }

        public PageableResponseBuilder withLimit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public PageableResponse build() {
            PageableResponse pageableResponse = new PageableResponse();
            pageableResponse.setData(data);
            pageableResponse.setLastValueId(lastValueId);
            pageableResponse.setLimit(limit);
            return pageableResponse;
        }
    }
}

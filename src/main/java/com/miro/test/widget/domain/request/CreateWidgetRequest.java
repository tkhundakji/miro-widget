package com.miro.test.widget.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class CreateWidgetRequest implements Serializable {
    @JsonProperty("coordinates")
    @NotNull(message = "Coordinates must be present")
    @Valid
    private CoordinateRequest coordinateRequest;

    @NotNull(message = "Height must be present")
    @JsonProperty("height")
    @Min(value = 1, message = "Height must be > 0")
    private Integer height;

    @NotNull(message = "Width must be present")
    @JsonProperty("width")
    @Min(value = 1, message = "Width must be > 0")
    private Integer width;

    @JsonProperty("zIndex")
    private Integer zIndex;

    public CreateWidgetRequest() {
    }

    public CreateWidgetRequest(@NotNull @Valid CoordinateRequest coordinateRequest, @NotNull Integer height, @NotNull Integer width, Integer zIndex) {
        this.coordinateRequest = coordinateRequest;
        this.height = height;
        this.width = width;
        this.zIndex = zIndex;
    }

    public CreateWidgetRequest(@NotNull @Valid CoordinateRequest coordinateRequest, @NotNull Integer height, @NotNull Integer width) {
        this.coordinateRequest = coordinateRequest;
        this.height = height;
        this.width = width;
    }

    public CoordinateRequest getCoordinateRequest() {
        return coordinateRequest;
    }

    public void setCoordinateRequest(@Valid CoordinateRequest coordinateRequest) {
        this.coordinateRequest = coordinateRequest;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @JsonProperty("zIndex")
    public Integer getZIndex() {
        return zIndex;
    }

    @JsonProperty("zIndex")
    public void setZIndex(Integer zIndex) {
        this.zIndex = zIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateWidgetRequest that = (CreateWidgetRequest) o;
        return Objects.equals(coordinateRequest, that.coordinateRequest) && Objects.equals(height, that.height) && Objects.equals(width, that.width) && Objects.equals(zIndex, that.zIndex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinateRequest, height, width, zIndex);
    }
}

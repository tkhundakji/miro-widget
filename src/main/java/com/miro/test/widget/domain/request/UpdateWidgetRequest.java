package com.miro.test.widget.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;

public class UpdateWidgetRequest {

    @JsonProperty("coordinates")
    private CoordinateRequest coordinateRequest;

    @JsonProperty("height")
    @Min(value = 1, message = "Height must be > 0")
    private Integer height;

    @JsonProperty("width")
    @Min(value = 1, message = "Width must be > 0")
    private Integer width;

    @JsonProperty("zIndex")
    private Integer zIndex;

    public UpdateWidgetRequest() {
    }

    public CoordinateRequest getCoordinates() {
        return coordinateRequest;
    }

    public void setCoordinates(CoordinateRequest coordinateRequest) {
        this.coordinateRequest = coordinateRequest;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getZIndex() {
        return zIndex;
    }

    public void setZIndex(Integer zIndex) {
        this.zIndex = zIndex;
    }

    public static final class UpdateWidgetRequestBuilder {
        private CoordinateRequest coordinateRequest;
        private Integer height;
        private Integer width;
        private Integer zIndex;

        private UpdateWidgetRequestBuilder() {
        }

        public static UpdateWidgetRequestBuilder anUpdateWidgetRequest() {
            return new UpdateWidgetRequestBuilder();
        }

        public UpdateWidgetRequestBuilder withCoordinateRequest(CoordinateRequest coordinateRequest) {
            this.coordinateRequest = coordinateRequest;
            return this;
        }

        public UpdateWidgetRequestBuilder withHeight(Integer height) {
            this.height = height;
            return this;
        }

        public UpdateWidgetRequestBuilder withWidth(Integer width) {
            this.width = width;
            return this;
        }

        public UpdateWidgetRequestBuilder withZIndex(Integer zIndex) {
            this.zIndex = zIndex;
            return this;
        }

        public UpdateWidgetRequest build() {
            UpdateWidgetRequest updateWidgetRequest = new UpdateWidgetRequest();
            updateWidgetRequest.setHeight(height);
            updateWidgetRequest.setWidth(width);
            updateWidgetRequest.setZIndex(zIndex);
            updateWidgetRequest.coordinateRequest = this.coordinateRequest;
            return updateWidgetRequest;
        }
    }
}

package com.miro.test.widget.domain.internal;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class WidgetModel implements Cloneable, Serializable {

    private UUID id;

    private CoordinateModel coordinates;

    private Integer height;

    private Integer width;

    private Integer zIndex;

    private LocalDate lastModified;

    public WidgetModel(CoordinateModel coordinates, Integer height, Integer width, Integer zIndex, LocalDate lastModified) {
        this.coordinates = coordinates;
        this.height = height;
        this.width = width;
        this.zIndex = zIndex;
        this.lastModified = lastModified;
    }

    public WidgetModel() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public CoordinateModel getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinateModel coordinates) {
        this.coordinates = coordinates;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getZIndex() {
        return zIndex;
    }

    public void setZIndex(Integer zIndex) {
        this.zIndex = zIndex;
    }

    public LocalDate getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDate lastModified) {
        this.lastModified = lastModified;
    }

    public WidgetModel clone() {
        try {
            return (WidgetModel) super.clone();
        } catch (CloneNotSupportedException e) {
            return new WidgetModel(this.getCoordinates().clone(), this.height, this.width, this.zIndex, LocalDate.ofEpochDay(this.lastModified.toEpochDay()));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WidgetModel that = (WidgetModel) o;
        return Objects.equals(id, that.id) && Objects.equals(coordinates, that.coordinates) && Objects.equals(height, that.height) && Objects.equals(width, that.width) && Objects.equals(zIndex, that.zIndex) && Objects.equals(lastModified, that.lastModified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, coordinates, height, width, zIndex, lastModified);
    }

    public static final class WidgetModelBuilder {
        private UUID id;
        private CoordinateModel coordinates;
        private Integer height;
        private Integer width;
        private Integer zIndex;
        private LocalDate lastModified;

        private WidgetModelBuilder() {
        }

        public static WidgetModelBuilder aWidgetModel() {
            return new WidgetModelBuilder();
        }

        public WidgetModelBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public WidgetModelBuilder withCoordinates(CoordinateModel coordinates) {
            this.coordinates = coordinates;
            return this;
        }

        public WidgetModelBuilder withHeight(Integer height) {
            this.height = height;
            return this;
        }

        public WidgetModelBuilder withWidth(Integer width) {
            this.width = width;
            return this;
        }

        public WidgetModelBuilder withZIndex(Integer zIndex) {
            this.zIndex = zIndex;
            return this;
        }

        public WidgetModelBuilder withLastModified(LocalDate lastModified) {
            this.lastModified = lastModified;
            return this;
        }

        public WidgetModel build() {
            WidgetModel widgetModel = new WidgetModel();
            widgetModel.setId(id);
            widgetModel.setCoordinates(coordinates);
            widgetModel.setHeight(height);
            widgetModel.setWidth(width);
            widgetModel.setZIndex(zIndex);
            widgetModel.setLastModified(lastModified);

            return widgetModel;
        }
    }
}

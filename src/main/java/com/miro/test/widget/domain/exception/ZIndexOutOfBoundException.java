package com.miro.test.widget.domain.exception;

public class ZIndexOutOfBoundException extends RuntimeException {
    public ZIndexOutOfBoundException() {
        super("Z-index is out of bound, please delete/update some widgets to make space");
    }
}

package com.miro.test.widget.controller;

import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import com.miro.test.widget.domain.response.PageableResponse;
import com.miro.test.widget.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/widgets")
@Validated
public class WidgetController {

    private final WidgetService widgetService;

    @Autowired
    public WidgetController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<WidgetModel> getWidget(@PathVariable("id") UUID id) {
        WidgetModel widget = this.widgetService.getWidget(id);
        if (widget == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(widget, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<PageableResponse<WidgetModel>> getWidgets(@RequestParam(required = false, name = "lastZIndex") Integer lastZIndex,
                                                                    @RequestParam(required = false, name = "limit", defaultValue = "10")
                                                                    @Max(value = 500, message = "Limit must be less than or equal to 500") Integer limit) {

        List<WidgetModel> widgets = this.widgetService.getWidgetsByZIndexOrder(lastZIndex, limit);
        if (widgets.isEmpty()) {
            return new ResponseEntity<PageableResponse<WidgetModel>>(
                    new PageableResponse<WidgetModel>(widgets, null, limit), HttpStatus.OK);
        }

        return new ResponseEntity<PageableResponse<WidgetModel>>(
                new PageableResponse<WidgetModel>(widgets, widgets.get(widgets.size() - 1).getZIndex(), limit),
                HttpStatus.OK
        );
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<WidgetModel> createWidget(@RequestBody @Valid CreateWidgetRequest widgetRequest) {
        WidgetModel widget = this.widgetService.storeWidget(widgetRequest);

        return new ResponseEntity<>(widget, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<WidgetModel> updateWidget(@PathVariable("id") UUID id, @RequestBody @Valid UpdateWidgetRequest updateWidgetRequest) {
        WidgetModel widget = this.widgetService.getWidget(id);
        if (widget == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        WidgetModel updatedWidget = widgetService.updateWidget(id, updateWidgetRequest);
        return new ResponseEntity<>(updatedWidget, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Void> deleteWidget(@PathVariable("id") UUID id) {
        WidgetModel widget = this.widgetService.getWidget(id);
        if (widget == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        if (this.widgetService.deleteWidget(id)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Void> deleteAll() {
        this.widgetService.deleteAll();

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}

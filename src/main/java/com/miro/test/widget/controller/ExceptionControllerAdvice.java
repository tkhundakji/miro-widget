package com.miro.test.widget.controller;

import com.miro.test.widget.domain.exception.WidgetNotFoundException;
import com.miro.test.widget.domain.exception.ZIndexOutOfBoundException;
import com.miro.test.widget.domain.response.ErrorDetailResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Date;

@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {WidgetNotFoundException.class})
    protected ResponseEntity<Error> handleWidgetNotfound(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();

        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ZIndexOutOfBoundException.class})
    protected ResponseEntity<Error> handleZIndexOutOfBound(RuntimeException ex, WebRequest request) {
        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Error> handleConstraintViolationExceptions(
            ConstraintViolationException ex) {

        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getBindingResult().toString());
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        // Do your code here
        ErrorDetailResponse errorDetails = new ErrorDetailResponse(new Date(), "Validation Failed",
                ex.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }
}

package com.miro.test.widget.service;

import com.miro.test.widget.domain.exception.WidgetNotFoundException;
import com.miro.test.widget.domain.internal.CoordinateModel;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import com.miro.test.widget.repository.WidgetRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class WidgetService {

    private final WidgetRepositoryInterface widgetRepositoryInterface;

    @Autowired
    public WidgetService(WidgetRepositoryInterface widgetRepositoryInterface) {
        this.widgetRepositoryInterface = widgetRepositoryInterface;
    }

    public WidgetModel updateWidget(UUID id, UpdateWidgetRequest updateWidgetRequest) {
        Optional<WidgetModel> newWidget = widgetRepositoryInterface.update(id, updateWidgetRequest);

        if (newWidget.isEmpty()) {
            throw new WidgetNotFoundException(id);
        }

        return newWidget.get();
    }

    public WidgetModel storeWidget(CreateWidgetRequest widgetRequest) {
        UUID id = UUID.randomUUID();

        WidgetModel widgetModel = WidgetModel.WidgetModelBuilder
                .aWidgetModel()
                .withId(id)
                .withCoordinates(CoordinateModel.CoordinateModelBuilder.aCoordinateModel()
                        .withX(widgetRequest.getCoordinateRequest().getX())
                        .withY(widgetRequest.getCoordinateRequest().getY())
                        .build())
                .withHeight(widgetRequest.getHeight())
                .withWidth(widgetRequest.getWidth())
                .withZIndex(widgetRequest.getZIndex()).build();

        return widgetRepositoryInterface.store(widgetModel);
    }

    public boolean deleteWidget(UUID id) {
        Optional<WidgetModel> widget = widgetRepositoryInterface.getWidgetById(id);

        if (widget.isEmpty()) {
            throw new WidgetNotFoundException(id);
        }

        return widgetRepositoryInterface.deleteWidgetById(id);
    }

    public void deleteAll() {
        widgetRepositoryInterface.deleteAll();
    }

    public WidgetModel getWidget(UUID id) {
        Optional<WidgetModel> widget = widgetRepositoryInterface.getWidgetById(id);

        if (widget.isEmpty()) {
            throw new WidgetNotFoundException(id);
        }

        return widget.get();
    }

    public List<WidgetModel> getWidgetsByZIndexOrder(@Nullable Integer lastZIndex, Integer limit) {
        return widgetRepositoryInterface.getWidgetsByZIndex(lastZIndex, limit);
    }
}

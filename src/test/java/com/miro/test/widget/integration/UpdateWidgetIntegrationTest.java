package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CoordinateRequest;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UpdateWidgetIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testCanUpdateAllElementsWithoutZ() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest();
        WidgetModel widgetModel = testClient.createWidget(createWidgetRequest);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withCoordinateRequest(new CoordinateRequest(-1, -2))
                .withWidth(widgetModel.getWidth().equals(100) ? 101 : 100)
                .withHeight(widgetModel.getWidth().equals(200) ? 201 : 200)
                .build();

        WidgetModel updatedWidget = testClient.updateWidget(widgetModel.getId(), updateWidgetRequest);

        assertEquals(updateWidgetRequest.getCoordinates().getX(), updatedWidget.getCoordinates().getX());
        assertEquals(updateWidgetRequest.getCoordinates().getY(), updatedWidget.getCoordinates().getY());
        assertEquals(updateWidgetRequest.getHeight(), updatedWidget.getHeight());
        assertEquals(updateWidgetRequest.getWidth(), updatedWidget.getWidth());
        assertEquals(widgetModel.getZIndex(), updatedWidget.getZIndex());
        assertEquals(widgetModel.getId(), updatedWidget.getId());
    }

    @Test
    public void testCanUpdateZIndex() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        WidgetModel widgetModel = testClient.createWidget(createWidgetRequest);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withCoordinateRequest(new CoordinateRequest(-1, -2))
                .withWidth(widgetModel.getWidth().equals(100) ? 101 : 100)
                .withHeight(widgetModel.getWidth().equals(200) ? 201 : 200)
                .withZIndex(2)
                .build();

        WidgetModel updatedWidget = testClient.updateWidget(widgetModel.getId(), updateWidgetRequest);

        assertEquals(updateWidgetRequest.getCoordinates().getX(), updatedWidget.getCoordinates().getX());
        assertEquals(updateWidgetRequest.getCoordinates().getY(), updatedWidget.getCoordinates().getY());
        assertEquals(updateWidgetRequest.getHeight(), updatedWidget.getHeight());
        assertEquals(updateWidgetRequest.getWidth(), updatedWidget.getWidth());
        assertEquals(updateWidgetRequest.getZIndex(), updatedWidget.getZIndex());
        assertEquals(widgetModel.getId(), updatedWidget.getId());
    }

    @Test
    public void testUpdateShiftValueIfExist() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(3);

        CreateWidgetRequest createWidgetRequest4 = WidgetTestUtil.generateValidCreateWidgetRequest(10);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 = testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 = testClient.createWidget(createWidgetRequest3);
        WidgetModel widget4 = testClient.createWidget(createWidgetRequest4);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withZIndex(2)
                .build();


        testClient.updateWidget(widget3.getId(), updateWidgetRequest);

        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(4, widgetModels.size());

        assertEquals(widget1.getZIndex(), widgetModels.get(0).getZIndex());
        assertEquals(widget1.getId(), widgetModels.get(0).getId());

        assertEquals(updateWidgetRequest.getZIndex(), widgetModels.get(1).getZIndex());
        assertEquals(widget3.getId(), widgetModels.get(1).getId());

        assertEquals(widgetModels.get(2).getId(), widget2.getId());
        assertTrue(widgetModels.get(2).getZIndex() > widgetModels.get(1).getZIndex() &&
                widgetModels.get(2).getZIndex() < widgetModels.get(3).getZIndex()
        );

        assertEquals(widget4.getId(), widgetModels.get(3).getId());
        assertEquals(widget4.getZIndex(), widgetModels.get(3).getZIndex());
    }

    @Test
    public void testUpdateChangesTheMaxZ() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest();

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withZIndex(1000)
                .build();

        testClient.updateWidget(widget1.getId(), updateWidgetRequest);

        WidgetModel widget2 = testClient.createWidget(createWidgetRequest2);

        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(2, widgetModels.size());

        assertEquals(updateWidgetRequest.getZIndex(), widgetModels.get(0).getZIndex());
        assertEquals(widget1.getId(), widgetModels.get(0).getId());

        assertTrue(widgetModels.get(1).getZIndex() > widgetModels.get(0).getZIndex());
        assertEquals(widget2.getId(), widgetModels.get(1).getId());
    }

    @Test
    public void cantUpdateInvalidWithAndHeight() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 = testClient.createWidget(createWidgetRequest2);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withHeight(-1000)
                .build();

        UpdateWidgetRequest updateWidgetRequest2 = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withWidth(-1000)
                .build();

        Exception exception = assertThrows(TestClient.ValidationException.class, () -> {
            var response1 = testClient.updateWidgetAndGetResponse(widget1.getId(), updateWidgetRequest);

        });

        Exception exception2 = assertThrows(TestClient.ValidationException.class, () -> {
            var response2 = testClient.updateWidgetAndGetResponse(widget2.getId(), updateWidgetRequest2);

        });

        WidgetModel WidgetModel1Show = testClient.showWidget(widget1.getId());
        WidgetModel WidgetModel2Show = testClient.showWidget(widget2.getId());

        assertEquals(widget1, WidgetModel1Show);
        assertEquals(widget2, WidgetModel2Show);
    }

    @Test
    public void returns404WhenWidgetDoesNotExist() throws Exception {
        Exception exception = assertThrows(TestClient.ResourceNotFoundException.class, () -> {
            var response = testClient.updateWidgetAndGetResponse(UUID.randomUUID(),
                    UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest().build());
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        });
    }
}

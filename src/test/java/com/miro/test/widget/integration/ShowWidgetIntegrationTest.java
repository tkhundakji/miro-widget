package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CoordinateRequest;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShowWidgetIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testCanShowCreatedWidget() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(5);

        WidgetModel widgetModel =  testClient.createWidget(createWidgetRequest);

        WidgetModel widgetModel2 =  testClient.showWidget(widgetModel.getId());

        assertEquals(createWidgetRequest.getCoordinateRequest().getX(), widgetModel2.getCoordinates().getX());
        assertEquals(createWidgetRequest.getCoordinateRequest().getY(), widgetModel2.getCoordinates().getY());
        assertEquals(createWidgetRequest.getHeight(), widgetModel2.getHeight());
        assertEquals(createWidgetRequest.getWidth(), widgetModel2.getWidth());
        assertEquals( 5, widgetModel2.getZIndex());
        assertEquals(widgetModel.getId(), widgetModel2.getId());
    }

    @Test
    public void testUpdateIsReflectedInShow() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        WidgetModel widgetModel = testClient.createWidget(createWidgetRequest);

        UpdateWidgetRequest updateWidgetRequest = UpdateWidgetRequest.UpdateWidgetRequestBuilder.anUpdateWidgetRequest()
                .withCoordinateRequest(new CoordinateRequest(-1, -2))
                .withWidth(widgetModel.getWidth().equals(100) ? 101 : 100)
                .withHeight(widgetModel.getWidth().equals(200) ? 201 : 200)
                .withZIndex(2)
                .build();

        WidgetModel updatedWidget = testClient.updateWidget(widgetModel.getId(), updateWidgetRequest);

        WidgetModel widgetModel2 = testClient.showWidget(widgetModel.getId());

        assertEquals(updatedWidget, widgetModel2);

        assertEquals(updateWidgetRequest.getCoordinates().getX(), widgetModel2.getCoordinates().getX());
        assertEquals(updateWidgetRequest.getCoordinates().getY(), widgetModel2.getCoordinates().getY());
        assertEquals(updateWidgetRequest.getHeight(), widgetModel2.getHeight());
        assertEquals(updateWidgetRequest.getWidth(), widgetModel2.getWidth());
        assertEquals(updateWidgetRequest.getZIndex(), widgetModel2.getZIndex());
        assertEquals(widgetModel.getId(), widgetModel2.getId());
    }

    @Test
    public void returns404WhenWidgetDoesNotExist() throws Exception {
        Exception exception = assertThrows(TestClient.ResourceNotFoundException.class, () -> {
            var response = testClient.showWidgetAndGetResponse(UUID.randomUUID());
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        });
    }
}

package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListWidgetIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testCanListCreatedWidgets() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(3);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);

        List<WidgetModel> widgetModels =  testClient.listWidgets();

        assertEquals(3, widgetModels.size());

        assertEquals(widget1.getZIndex(), widgetModels.get(0).getZIndex());
        assertEquals(widget1.getId(), widgetModels.get(0).getId());

        assertEquals( widget2.getId(), widgetModels.get(1).getId());
        assertEquals(widget2.getZIndex(), widgetModels.get(1).getZIndex());

        assertEquals(widget3.getId(), widgetModels.get(2).getId() );
        assertEquals(widget3.getZIndex(), widgetModels.get(2).getZIndex());
    }

    @Test
    public void testPagingWorks() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(3);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);

        List<WidgetModel> widgetModels =  testClient.listWidgets(1, null);

        assertEquals(1, widgetModels.size());
        assertEquals(widget1.getZIndex(), widgetModels.get(0).getZIndex());

        widgetModels =  testClient.listWidgets(1, widget1.getZIndex());

        assertEquals(1, widgetModels.size());
        assertEquals(widget2.getZIndex(), widgetModels.get(0).getZIndex());

        widgetModels =  testClient.listWidgets(1, widget2.getZIndex());

        assertEquals(1, widgetModels.size());
        assertEquals(widget3.getZIndex(), widgetModels.get(0).getZIndex());

        widgetModels =  testClient.listWidgets(1, widget3.getZIndex());

        assertEquals(0, widgetModels.size());
    }

    @Test
    public void testSortingWorks() throws Exception {
        Random random = new Random();

        for(int i = 0 ; i < 10 ; i++) {
            CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(random.nextInt(1));
            testClient.createWidget(createWidgetRequest);
        }
        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(10, widgetModels.size());

        for(int i = 1; i < widgetModels.size() ; i ++) {
            assertTrue(widgetModels.get(i).getZIndex() > widgetModels.get(i -1).getZIndex());
        }
    }

    @Test
    public void testListingEmptyWidget() throws Exception {
        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(0, widgetModels.size());
    }
}

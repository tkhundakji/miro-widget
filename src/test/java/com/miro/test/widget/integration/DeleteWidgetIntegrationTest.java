package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DeleteWidgetIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testCanDeleteWidgetThatExist() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(5);

        WidgetModel widgetModel =  testClient.createWidget(createWidgetRequest);
        WidgetModel showWidgetModel = testClient.showWidget(widgetModel.getId());

        assertEquals(widgetModel, showWidgetModel);

        testClient.deleteWidget(widgetModel.getId());

        Exception exception = assertThrows(TestClient.ResourceNotFoundException.class, () -> {
            var response = testClient.showWidgetAndGetResponse(widgetModel.getId());
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        });
    }

    @Test
    public void returns404WhenWidgetDoesNotExist() throws Exception {
        Exception exception = assertThrows(TestClient.ResourceNotFoundException.class, () -> {
            var response = testClient.deleteWidgetAndGetResponse(UUID.randomUUID());
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        });
    }
}

package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClearWidgetIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testClearingWidgetsRemoveThem() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest();
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest();
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest();
        CreateWidgetRequest createWidgetRequest4 = WidgetTestUtil.generateValidCreateWidgetRequest();

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);
        WidgetModel widget4 =  testClient.createWidget(createWidgetRequest4);

        List<WidgetModel> widgetModels =  testClient.listWidgets();

        assertEquals(4, widgetModels.size());

        testClient.deleteAll();

        widgetModels =  testClient.listWidgets();
        assertEquals(0, widgetModels.size());
    }

    @Test
    public void testCreatingAfterDeletingAllRestsCounter() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest();
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest();

        testClient.createWidget(createWidgetRequest);
        testClient.createWidget(createWidgetRequest2);

        List<WidgetModel> widgetModels =  testClient.listWidgets();
        assertEquals(2, widgetModels.size());
        testClient.deleteAll();

        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest();
        CreateWidgetRequest createWidgetRequest4 = WidgetTestUtil.generateValidCreateWidgetRequest();

        testClient.createWidget(createWidgetRequest);
        testClient.createWidget(createWidgetRequest2);

        List<WidgetModel> widgetModels2 =  testClient.listWidgets();

        assertEquals(2, widgetModels2.size());

        assertEquals(widgetModels.get(0).getZIndex(), widgetModels2.get(0).getZIndex());
        assertEquals(widgetModels.get(1).getZIndex(), widgetModels2.get(1).getZIndex());
    }
}

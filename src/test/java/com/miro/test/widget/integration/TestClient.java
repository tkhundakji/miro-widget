package com.miro.test.widget.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import com.miro.test.widget.domain.request.UpdateWidgetRequest;
import com.miro.test.widget.domain.response.PageableResponse;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClient {
    private final int port;
    private final ObjectMapper objectMapper;

    public TestClient(int port, ObjectMapper objectMapper) {
        this.port = port;
        this.objectMapper = objectMapper;
        this.restTemplate = new TestRestTemplate();

        this.restTemplate.getRestTemplate().setErrorHandler(new ClientErrorHandler());
    }

    private static String baseUrl = "api/v1/widgets";

    TestRestTemplate restTemplate = new TestRestTemplate();

    public void deleteAll() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort(baseUrl), HttpMethod.DELETE, entity, Void.class);
        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);
    }

    public WidgetModel createWidget(CreateWidgetRequest createWidgetRequest) throws JsonProcessingException {
        var response = createWidgetAndGetResponse(createWidgetRequest);

        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);
        return response.getBody();
    }

    public ResponseEntity<WidgetModel> createWidgetAndGetResponse(CreateWidgetRequest createWidgetRequest) throws JsonProcessingException {
        RequestEntity<String> requestEntity = RequestEntity.post(createURLWithPort(baseUrl)).contentType(MediaType.APPLICATION_JSON)
                .body(objectMapper.writeValueAsString(createWidgetRequest));

        return restTemplate.exchange(requestEntity, WidgetModel.class);
    }

    public WidgetModel updateWidget(UUID id, UpdateWidgetRequest updateWidgetRequest) throws JsonProcessingException {
        var response = updateWidgetAndGetResponse(id, updateWidgetRequest);

        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);

        return response.getBody();
    }

    public ResponseEntity<WidgetModel> updateWidgetAndGetResponse(UUID id, UpdateWidgetRequest updateWidgetRequest) throws JsonProcessingException {
        RequestEntity<String> requestEntity = RequestEntity.put(createURLWithPort(baseUrl + "/" + id.toString())).contentType(MediaType.APPLICATION_JSON)
                .body(objectMapper.writeValueAsString(updateWidgetRequest));

        return restTemplate.exchange(requestEntity, WidgetModel.class);
    }

    public Void deleteWidget(UUID id) throws JsonProcessingException {
        var response = deleteWidgetAndGetResponse(id);

        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);

        return response.getBody();
    }

    public ResponseEntity<Void> deleteWidgetAndGetResponse(UUID id) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        return restTemplate.exchange(createURLWithPort(baseUrl + "/" + id.toString()), HttpMethod.DELETE, entity, Void.class);
    }

    public WidgetModel showWidget(UUID id) throws JsonProcessingException {
        var response = showWidgetAndGetResponse(id);

        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);

        return response.getBody();
    }

    public ResponseEntity<WidgetModel> showWidgetAndGetResponse(UUID id) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        return restTemplate.exchange(createURLWithPort(baseUrl + "/" + id.toString()), HttpMethod.GET, entity, WidgetModel.class);
    }

    public List<WidgetModel> listWidgets() throws JsonProcessingException {
        return listWidgets(null, null);
    }

    public List<WidgetModel> listWidgets(Integer limit, Integer lastZIndex) throws JsonProcessingException {

        var response = listWidgetsAndGetResponse(limit, lastZIndex);

        assertEquals(response.getStatusCode().series(), HttpStatus.Series.SUCCESSFUL);

        return objectMapper.readValue(response.getBody(), new TypeReference<PageableResponse<WidgetModel>>() {
        }).getData();
    }

    public ResponseEntity<String> listWidgetsAndGetResponse() throws JsonProcessingException {
        return listWidgetsAndGetResponse(null, null);
    }

    public ResponseEntity<String> listWidgetsAndGetResponse(Integer limit, Integer lastZIndex) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        String url = createURLWithPort(baseUrl);

        if (limit != null) {
            url = url + "?limit=" + limit;
            if (lastZIndex != null) {
                url = url + "&lastZIndex=" + lastZIndex;
            }
        } else if (lastZIndex != null) {
            url = url + "?lastZIndex=" + lastZIndex;
        }

        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    static class ResourceNotFoundException extends RuntimeException {

    }

    static class ValidationException extends RuntimeException {

    }

    private static class ClientErrorHandler implements ResponseErrorHandler {
        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
            if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new ResourceNotFoundException();
            }

            if (response.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new ValidationException();
            }
            // handle other possibilities, then use the catch all...
            throw new HttpClientErrorException(response.getStatusCode());
        }

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            return response.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR
                    || response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR;
        }
    }
}

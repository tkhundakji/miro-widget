package com.miro.test.widget.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miro.test.widget.WidgetTestUtil;
import com.miro.test.widget.domain.internal.WidgetModel;
import com.miro.test.widget.domain.request.CreateWidgetRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CreateWidgetIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private TestClient testClient;


    @BeforeEach
    void deleteData() {
        this.testClient = new TestClient(this.port, this.objectMapper);

        testClient.deleteAll();
    }

    @Test
    public void testCrateWidgetWithoutZIndex() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest();

        WidgetModel widgetModel =  testClient.createWidget(createWidgetRequest);

        assertEquals(createWidgetRequest.getCoordinateRequest().getX(), widgetModel.getCoordinates().getX());
        assertEquals(createWidgetRequest.getCoordinateRequest().getY(), widgetModel.getCoordinates().getY());
        assertEquals(createWidgetRequest.getHeight(), widgetModel.getHeight());
        assertEquals(createWidgetRequest.getWidth(), widgetModel.getWidth());
        assertNotNull(widgetModel.getZIndex());
        assertNotNull(widgetModel.getId());

        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(1,widgetModels.size());
    }

    @Test
    public void testCrateWidgetWithZIndex() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(5);

        WidgetModel widgetModel =  testClient.createWidget(createWidgetRequest);

        assertEquals(createWidgetRequest.getCoordinateRequest().getX(), widgetModel.getCoordinates().getX());
        assertEquals(createWidgetRequest.getCoordinateRequest().getY(), widgetModel.getCoordinates().getY());
        assertEquals(createWidgetRequest.getHeight(), widgetModel.getHeight());
        assertEquals(createWidgetRequest.getWidth(), widgetModel.getWidth());
        assertEquals(widgetModel.getZIndex(), 5);
        assertNotNull(widgetModel.getId());

        List<WidgetModel> widgetModels = testClient.listWidgets();

        assertEquals(1,widgetModels.size());
    }

    @Test
    public void testCreateShiftsToTheEnd() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(10);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);

        List<WidgetModel> widgetModels =  testClient.listWidgets();

        assertEquals(3, widgetModels.size());

        assertEquals(widgetModels.get(0).getZIndex(), widget2.getZIndex());

        assertEquals(widgetModels.get(1).getId(), widget1.getId());
        assertEquals(widgetModels.get(1).getZIndex(),2);

        assertEquals(widgetModels.get(2).getId(), widget3.getId());
        assertEquals(widgetModels.get(2).getZIndex(),widget3.getZIndex());
    }

    @Test
    public void testCreateUpdatesTheMaxIndex() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(1000);
        CreateWidgetRequest createWidgetRequest4 = WidgetTestUtil.generateValidCreateWidgetRequest();

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);
        WidgetModel widget4 =  testClient.createWidget(createWidgetRequest4);

        List<WidgetModel> widgetModels =  testClient.listWidgets();

        assertEquals(4, widgetModels.size());

        assertEquals(widgetModels.get(0).getId(), widget1.getId());
        assertEquals(1, widgetModels.get(0).getZIndex());


        assertEquals(widgetModels.get(1).getId(), widget2.getId());
        assertEquals(2, widgetModels.get(1).getZIndex());

        assertEquals(widgetModels.get(2).getId(), widget3.getId());
        assertEquals(1000, widgetModels.get(2).getZIndex());

        assertEquals(widgetModels.get(3).getId(), widget4.getId());
        assertTrue(widgetModels.get(3).getZIndex() > 1000);
    }

    @Test
    public void testOnlyWhatIsNeededIsShifted() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest2 = WidgetTestUtil.generateValidCreateWidgetRequest(2);
        CreateWidgetRequest createWidgetRequest3 = WidgetTestUtil.generateValidCreateWidgetRequest(3);

        CreateWidgetRequest createWidgetRequest4 = WidgetTestUtil.generateValidCreateWidgetRequest(6);
        CreateWidgetRequest createWidgetRequest5 = WidgetTestUtil.generateValidCreateWidgetRequest(7);
        CreateWidgetRequest createWidgetRequest6 = WidgetTestUtil.generateValidCreateWidgetRequest(8);

        CreateWidgetRequest createWidgetRequest7 = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        CreateWidgetRequest createWidgetRequest8 = WidgetTestUtil.generateValidCreateWidgetRequest(12);

        WidgetModel widget1 = testClient.createWidget(createWidgetRequest);
        WidgetModel widget2 =  testClient.createWidget(createWidgetRequest2);
        WidgetModel widget3 =  testClient.createWidget(createWidgetRequest3);
        WidgetModel widget4 =  testClient.createWidget(createWidgetRequest4);
        WidgetModel widget5 =  testClient.createWidget(createWidgetRequest5);
        WidgetModel widget6 =  testClient.createWidget(createWidgetRequest6);
        WidgetModel widget7 =  testClient.createWidget(createWidgetRequest7);
        WidgetModel widget8 =  testClient.createWidget(createWidgetRequest8);

        List<WidgetModel> widgetModels =  testClient.listWidgets();

        assertEquals(8 , widgetModels.size());

        assertEquals(widgetModels.get(0).getId(), widget7.getId());
        assertEquals(1, widgetModels.get(0).getZIndex());

        assertEquals(widgetModels.get(1).getId(), widget1.getId());
        assertEquals(widgetModels.get(2).getId(), widget2.getId());
        assertEquals(widgetModels.get(3).getId(), widget3.getId());

        assertEquals(widgetModels.get(4), widget4);
        assertEquals(widgetModels.get(5), widget5);
        assertEquals(widgetModels.get(6), widget6);
        assertEquals(widgetModels.get(7), widget8);
    }

    @Test
    public void cantCreateWidgetWithNegativeHeight() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        createWidgetRequest.setHeight(-1);

        Exception exception = assertThrows(TestClient.ValidationException.class, () -> {
            var response = testClient.createWidgetAndGetResponse(createWidgetRequest);

        });
    }

    @Test
    public void cantCreateWidgetWithNegativeWidth() throws Exception {
        CreateWidgetRequest createWidgetRequest = WidgetTestUtil.generateValidCreateWidgetRequest(1);
        createWidgetRequest.setWidth(-1);

        Exception exception = assertThrows(TestClient.ValidationException.class, () -> {
            var response = testClient.createWidgetAndGetResponse(createWidgetRequest);
        });
    }
}

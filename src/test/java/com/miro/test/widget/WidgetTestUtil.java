package com.miro.test.widget;

import com.miro.test.widget.domain.request.CoordinateRequest;
import com.miro.test.widget.domain.request.CreateWidgetRequest;

import java.util.Random;

public class WidgetTestUtil {

    private static Random random = new Random();
    public static CreateWidgetRequest generateValidCreateWidgetRequest() {
        return new CreateWidgetRequest(new CoordinateRequest(
                random.nextInt(1000),
                random.nextInt(1000)
        ), random.nextInt(1000),random.nextInt(1000), null);
    }

    public static CreateWidgetRequest generateValidCreateWidgetRequest(Integer zIndex) {
        return new CreateWidgetRequest(new CoordinateRequest(
                random.nextInt(1000),
                random.nextInt(1000)
        ), random.nextInt(1000),random.nextInt(1000), zIndex);
    }
}

# Widget Service

Widget service, allows REST operations on widgets.


## Optional requirements implemented

1) Paging
2) Sql storage implementation

## Usage

To create a widget POST -> {{host}}:9001/api/v1/widgets
```json
{
    "coordinates": {
        "x": 5,
        "y": 5
    }, 
    "height": 100,
    "width": 112,
    "zIndex": 4
}
```

To update a widget PUT-> {{host}}:9001/api/v1/widgets/{id}
```json
{
    "zIndex": 1000,
    "coordinates": {
        "y": 10
    }
    
}
```


To delete a widget DELETE -> {{host}}:9001/api/v1/widgets/{id}

To delete all widgets  DELETE -> {{host}}:9001/api/v1/widgets

To show a widget GET-> {{host}}:9001/api/v1/widgets/{id}

To list widget GET-> {{host}}:9001/api/v1/widgets?limit=100&lastZIndex=-1

## Running the application

For the sql implementation
```bash
 mvn spring-boot:run -Dspring-boot.run.profiles=sql
```
For the in-memory implementation
```bash
 mvn spring-boot:run -Dspring-boot.run.profiles=memory
```

## Followup

To reduce the chance of having to shift, a background task can be added to remap the values for example 1,2,10 can become 1, 100, 200. was not sure if that it is allowed thus I did not implement it

## Test Coverage 

Only integration tests were created, coverage > 50%


